// CheckboxUI v1.0.0 - jQuery checkbox plugin
// (c) 2012 BlooApple
// License: http://www.opensource.org/licenses/mit-license.php

//This plugin help to customize the checkbox interface and have an additional method for reseting checkbox by selector when 
//the callee is stored within a var.

(function( $ ){

  $.fn.checkboxUI = function(options) {

    var defaults = {
        activeClass: "CheckBoxUIActive", //will be added on label which associated with checked checkbox
        disableClass: "Disable", //will be added on label which associated with checked checkbox
        onAfterClick: function(inputObj, lblObj){}, //Do something after click
        labelSelector: "", //In case of using some really special input. Wil be implemented on v2.0
        development: true
    },
    _this = this;

    var options = $.extend(defaults, options);

    //Looking for the label which stay next or before the input. And make sure the current label have the right "for" attribute.
    function matchingLabel(inputObj){
        var inputLbl = jQuery(inputObj).next();
        if(inputLbl.attr("for") == inputObj.id)
            return inputLbl;
        inputLbl = jQuery(inputObj).prev();
        if(inputLbl.attr("for") == inputObj.id)
            return inputLbl;
        return undefined;
    }

    jQuerycheckboxUI = function (jEl) {
        var lbl = matchingLabel(jEl);
        if(lbl==undefined)
        {
            if(options.development)
            {
                jQuery("body").prepend("<div class='checkboxError'>The check box with id='"+jEl.id+"' does not have any label (which is be fore or next to it) associated with.</div>")
                return false;
            }
        }

        //init checkbox with the current value and store the orignal value within the node
        
        if(jEl.checked)
        {
            lbl.addClass(options.activeClass);
            jQuery(jEl).data("origin-val","checked");
        }
        else{
            lbl.removeClass(options.activeClass);
            jQuery(jEl).data("origin-val","");
        }
        jQuery(jEl).addClass("Initialized");

        lbl.bind("click.checkboxUI",function(e){
            e.preventDefault();
            if ( !jEl.checked == true && !jEl.disabled) {                 
                jEl.checked = "checked";
                lbl.addClass(options.activeClass);
            }
            else {
                jEl.checked = "";
                lbl.removeClass(options.activeClass);
            }

            (typeof options.onAfterClick=="function") && options.onAfterClick( jEl,lbl.get(0));
            e.stopPropagation();
        })
    }

    this.reset = function(){
        this.each(function(){
            this.reset(); 
        });
    };

    this.each(function() {
        if(this.tagName == "INPUT")
            new jQuerycheckboxUI(this);
        else
            return;
        //the reset method use the data store in the input, the orignal value when load page.
        this.reset = function(){
            this.checked = jQuery(this).data("origin-val");
            if(!this.checked)
                matchingLabel(this).removeClass(options.activeClass);
            else
                matchingLabel(this).addClass(options.activeClass);
        };
        this.enable = function(){
            this.disabled = false;
            matchingLabel(this).removeClass(options.disableClass);
        }
        this.disable = function(){
            this.disabled = true;
            matchingLabel(this).addClass(options.disableClass);
        }
    });

    //Loop through the set to find how many forms the checkbox stay in.
    var CBArray = {};
    _this.each(function(){
        var tempId = this.form.id;
        if(CBArray[tempId]==undefined)
            CBArray[tempId]=[];
        CBArray[tempId].push(this);
    });
    jQuery.each(CBArray,function(key,val){
        jQuery("#"+key).bind("reset.checkbox",function(){
            for(var i=0; i< val.length; i++)
            {
                val[i].reset();
            }
        });
    })

    return this;
  };
})( jQuery );
